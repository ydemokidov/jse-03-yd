package com.t1.yd.tm;

import static com.t1.yd.tm.constant.TerminalConstant.*;

public class TaskManager {

    public static void main(String[] args) {
        showWelcome();
        processArguments(args);
    }

    private static void processArguments(String[] args){
        if(args == null || args.length==0) return;
        processArgument(args[0]);
    }

    private static void processArgument(String arg){
        switch(arg){
            case ABOUT:
                showAbout();
                break;
            case VERSION:
                showVersion();
                break;
            case HELP:
                showHelp();
                break;
            default:
                showError();
        }
    }

    private static void showAbout(){
        System.out.println("[ABOUT]");
        System.out.println("Author: Yuriy Demokidov");
        System.out.println("Email: ydemokidov@t1-consulting.ru");
    }

    private static void showVersion(){
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    private static void showHelp(){
        System.out.println("[HELP]");
        System.out.printf("%s - Show info about program \n", ABOUT);
        System.out.printf("%s - Show program version \n", VERSION);
        System.out.printf("%s - Show arguments list \n", HELP);
    }

    private static void showWelcome(){
        System.out.println("*** WELCOME TO TASK MANAGER ***");
    }

    private static void showError(){
        System.err.println("[ERROR]");
        System.err.println("Argument is not supported");
    }

}
